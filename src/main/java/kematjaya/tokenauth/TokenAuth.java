/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package kematjaya.tokenauth;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpRequest.BodyPublishers;

/**
 *
 * @author programmer
 */
public class TokenAuth {

    String token = "";
    HttpClient httpClient;
    
    public TokenAuth() {
        this.httpClient = HttpClient.newHttpClient();
    }
    
    public static void main(String[] args) throws IOException, InterruptedException
    {
        TokenAuth auth = new TokenAuth();
        
        JSONObject products = auth.getProducts();
        
        System.out.println(products);
    }
    
    protected JSONObject getProducts() throws IOException, InterruptedException
    {
        if (this.token.isEmpty()) {
            this.token = this.getJSONToken();
        }
        
        String url = "http://pos.demo.be-tech.id/api/2/product.html";
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .GET()
                .header("Authorization", "Bearer " + this.token)
                .build();
        HttpResponse<String> response = this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        JSONParser parser = new JSONParser();
        try {
            
            return (JSONObject)parser.parse(response.body());
        } catch (ParseException pe) {
            JSONObject error = new JSONObject();
            error.put("error", pe.getMessage());
            
            return error;
        }
    }
    
    protected String getJSONToken() throws IOException, InterruptedException
    {
        String username = "root";
        String password = "admin123";
        String url = "http://pos.demo.be-tech.id/api/login_check";
        JSONObject json = new JSONObject();
        json.put("username", username);
        json.put("password", password);
         
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .POST(BodyPublishers.ofString(json.toJSONString()))
                .header("Content-Type", "application/json")
                .build();
        
        HttpResponse<String> response = this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        
        JSONParser parser = new JSONParser();
        try {
            
            JSONObject obj = (JSONObject)parser.parse(response.body());
            
            return (String) obj.get("token");
        } catch (ParseException pe) {
            return pe.getMessage();
        }
    }
}
